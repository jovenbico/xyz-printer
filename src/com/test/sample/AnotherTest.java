package com.test.sample;

public class AnotherTest {

	public static void main(String[] args) {
		int dim = 5;

		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				if (i == j || i + j == dim - 1) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}

			System.out.println();
		}

		System.out.println("\t");

		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				if (((i == j || i + j == dim - 1) && i < dim / 2 + 1) || (i > dim / 2 && j == dim / 2)) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}

			System.out.println();
		}

		System.out.println("\t");

		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				if (i == 0 || i + j == dim - 1 || i == dim - 1) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}

			System.out.println();
		}
	}

}
