package com.test.factory;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class YPrinter extends AbstractPrinter implements PrintProcessor {

	@Override
	public void print(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (((i == j || i + j == n - 1) && i < n / 2 + 1) || (i > n / 2 && j == n / 2)) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}

			System.out.println();
		}
		System.out.println("\n");

	}
	
	@Override
	public String print2(int n) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		PrintStream old = System.out;
		System.setOut(ps);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (((i == j || i + j == n - 1) && i < n / 2 + 1) || (i > n / 2 && j == n / 2)) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}

			System.out.println();
		}
		System.out.println("\n");
		System.out.flush();
		System.setOut(old);
		return baos.toString();
	}

}
