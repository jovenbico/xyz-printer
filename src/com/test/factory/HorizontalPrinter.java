package com.test.factory;

public class HorizontalPrinter extends AbstractPrinter implements PrintProcessor {

	@Override
	public void print(int n) {
		String xPrinter = new XPrinter().print2(n);
		String yPrinter = new YPrinter().print2(n);
		String zPrinter = new ZPrinter().print2(n);
		System.out.println(xPrinter + "\t" + yPrinter + "\t" + zPrinter);

	}

}
