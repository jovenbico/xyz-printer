package com.test.factory;

public class JPrinter extends AbstractPrinter implements PrintProcessor {
	
	@Override
	public void print(int n) {
		System.out.println("printing J "+n+" times");
	}

}
