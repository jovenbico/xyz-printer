package com.test.factory;

public class PrintFacatory {

	public static PrintProcessor getPrinter(String letter) {
		switch (letter.toLowerCase()) {
		case "x":
			return new XPrinter();
		case "y":
			return new YPrinter();
		case "z":
			return new ZPrinter();
		case "j":
			return new JPrinter();
		case "vertical":
			return new VerticalPrinter();
		case "horizontal":
			return new HorizontalPrinter();
		default:
			return new AbstractPrinter();
		}
	}
	
	public static PrintProcessor justPrint() {
		return new AbstractPrinter();
	}

}
